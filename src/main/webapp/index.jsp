<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="title" scope="request" value="Information System" />
<html>
<%@ include file="layout/head.jsp" %>
<body>
<%@ include file="layout/header.jsp" %>
<div class="container-fluid">
    <c:choose>
        <%--@elvariable id="logged" type="tdorzhi.homework2.pojo.User"--%>
        <%--@elvariable id="map" type="java.util.Map"--%>
        <c:when test="${logged != null}">
            <form action="/list/create/-1" method="post">
                <input id="createListName" name="name" type="text" value="" required />
                <input name="userId" type="hidden" value="${logged.id}" />
                <button class="btn btn-info" type="submit">Add list</button>
            </form>
            <c:if test="${map != null && map.size() > 0}">
                <c:forEach items="${map}" var="entry">
                    <c:set var="list" value="${entry.key}"/>
                    <c:set var="items" value="${entry.value}"/>
                    <%@ include file="layout/list/list.jsp" %>
                </c:forEach>
            </c:if>
        </c:when>
        <c:otherwise>
            <h1 class="h1 text-center text-nowrap">Why not <a href="user/register">sign up</a>?</h1>
        </c:otherwise>
    </c:choose>
</div>
<%@ include file="layout/footer.jsp" %>
</body>
</html>
