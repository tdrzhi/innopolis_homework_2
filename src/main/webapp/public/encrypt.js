/**
 * Created by dixi on 1/5/17.
 *
 */
var formTag = $('form[id="authorize"]');
//    var inputNameTag = formTag.find('input[name="name"]'); shall we use it as a salt part?
var inputEmailTag = formTag.find('input[name="email"]');
var inputPasswordTag = formTag.find('input[name="password"]');
var submitBtn = formTag.find('button[type="submit"]');
var iterations = 1000;
var keysize = 16;

submitBtn.click(function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    encrypt();
});

function encrypt() {
    var salt = "" + inputEmailTag.val();
    var pass = "" + inputPasswordTag.val();
    var mypbkdf2 = new PBKDF2(pass, salt, iterations, keysize);
    var status_callback = function(percent_done) {
    };
    var result_callback = function(key) {
        inputPasswordTag.val(key);
        formTag.submit();
    };
    mypbkdf2.deriveKey(status_callback, result_callback);
    return false;
}