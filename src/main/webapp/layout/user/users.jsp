<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("title", "All users"); %>
<html>
<%@ include file="/layout/head.jsp" %>
<body>
<%@ include file="/layout/header.jsp" %>
<div class="container">
    <div class="col-md-offset-2 col-md-8">

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Password</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <c:if test="${attachment != null && attachment.getClass().isArray() }">
                <c:forEach items="${attachment}" var="user" >
                    <c:choose>
                        <c:when test="${user.enabled == true}">
                            <c:set var="actionVal" value="block" />
                            <c:set var="actionText" value="Block" />
                        </c:when>
                        <c:otherwise>
                            <c:set var="actionVal" value="unblock" />
                            <c:set var="actionText" value="Unblock" />
                        </c:otherwise>
                    </c:choose>
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.name}</td>
                        <td>${user.email}</td>
                        <td>${user.password}</td>
                        <td>
                            <form action="/user/${actionVal}/${user.id}" method="get">
                                <button>${actionText}</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
            </tbody>
        </table>
    </div>

</div>
<%@ include file="/layout/footer.jsp" %>
</body>
</html>