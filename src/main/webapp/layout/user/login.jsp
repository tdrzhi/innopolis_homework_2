<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("title", "Information System"); %>
<html>
<%@ include file="../head.jsp" %>
<body>
<%@ include file="../header.jsp" %>

<div class="container-fluid col-md-offset-4 col-md-4">
    <form id="authorize" class="form-signin" action="login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <br />
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required>
       <%-- <div class="checkbox">
            <label>
                <input name="stay_online" type="checkbox" value="true"> Remember me
            </label>
        </div>--%>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>
<%@ include file="../footer.jsp" %>
<script src="/public/sha1.js" ></script>
<script src="/public/pbkdf2.js" ></script>
<script src="/public/encrypt.js" ></script>
</body>
</html>
