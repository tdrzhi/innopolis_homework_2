<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("title", "Registration");%>
<html>
<%@ include file="../head.jsp" %>
<body>
<%@ include file="../header.jsp" %>
<div class="container-fluid col-md-offset-4 col-md-4">
    <form id="authorize" class="form-signin" action="/user/register" method="post">
        <h2 class="form-signin-heading">Please, fill in registration data</h2>
        <label for="inputName" class="sr-only">Email address</label>
        <input type="text" name="name" id="inputName" class="form-control" placeholder="Username" required autofocus>
        <br />
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required>
        <br />
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
</div>
<%@ include file="../footer.jsp" %>
<script src="/public/sha1.js" ></script>
<script src="/public/pbkdf2.js" ></script>
<script src="/public/encrypt.js" ></script>
</body>
</html>
