<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <c:choose>
            <%--@elvariable id="logged" type="tdorzhi.homework2.pojo.User"--%>
            <c:when test="${logged != null}">
                <p class="navbar-text navbar-left"><span class="label label-success">${logged.email}</span></p>
                <form action="/user/logout" method="post" >
                    <button type="submit" class="btn btn-danger navbar-btn">Logout</button>
                </form>
            </c:when>
            <c:otherwise>
                <form action="/user/login" method="get" >
                    <button type="submit" class="btn btn-info navbar-btn">Sign in</button>
                </form>
                <form action="/user/register" method="get" >
                    <button type="submit" class="btn btn-primary navbar-btn">Sign up</button>
                </form>
            </c:otherwise>
        </c:choose>
    </div>
</nav>