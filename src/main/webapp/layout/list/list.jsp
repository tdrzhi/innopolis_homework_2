<div class="panel panel-default ">
    <div class="panel-heading">
        <%--@elvariable id="list" type="tdorzhi.homework2.pojo.BuyList"--%>
        <h4>${list.name}</h4>
        <form action="/list/delete/${list.id}" method="post">
            <button type="submit" name="listId" value="" class="btn btn-default btn-danger">Delete</button>
        </form>
        <form action="/list/update/${list.id}" method="post">
            <input id="updateListName" type="text" name="name" />
            <button type="submit" name="listId" value="" class="btn btn-default btn-info">Rename</button>
        </form>
    </div>
    <div class="panel-body">
        <form action="/item/create/-1" method="post">
            <label for="createItemName">Name</label>
            <input id="createItemName" type="text" name="name" />
            <label for="createItemBudget">Budget</label>
            <input id="createItemBudget" type="text" name="budget" />
            <input id="createItemListId" type="hidden" name="listId" value="${list.id}" />
            <button type="submit" class="btn btn-info">Add</button>
        </form>
        <c:if test="${items != null}">
            <ul>
                <c:forEach items="${items}" var="item">
                    <li><%--@elvariable id="item" type="tdorzhi.homework2.pojo.Item"--%>
                        <c:choose>
                            <c:when test="${item.enabled == true}" >
                                <h5 class="h5 text-success">${item.name} ${item.budget}</h5>
                                <form action="/item/update/${item.id}" method="post">
                                    <input type="hidden" name="enabled" value="false" >
                                    <button type="submit" class="btn btn-warning">Mark Done</button>
                                </form>
                            </c:when>
                            <c:otherwise>
                                <h5 class="h5 text-muted">${item.name} ${item.budget}</h5>
                                <form action="/item/update/${item.id}" method="post">
                                    <input type="hidden" name="enabled" value="true" >
                                    <button type="submit" class="btn btn-warning">Mark Undone</button>
                                </form>
                            </c:otherwise>
                        </c:choose>
                        <%--
                        <form action="/item/update/${item.id}" method="post">
                            <label for="updateItemName">Item name</label>
                            <input id="updateItemName" type="text" name="name" value="" required>
                            <label for="updateItemBudget">Item budget</label>
                            <input id="updateItemBudget" type="number" name="budget" value="" >
                            <button type="submit" class="btn btn-default">Edit</button>
                        </form>--%>
                        <form action="/item/delete/${item.id}" method="post">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
    </div>
</div>