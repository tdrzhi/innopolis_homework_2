<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("title", "Information System"); %>
<html>
<%@ include file="layout/head.jsp" %>
<body>
<%@ include file="layout/header.jsp" %>
<%--@elvariable id="exception" type="Exception"--%>
<c:out value="${exception}" />
<%@ include file="layout/footer.jsp" %>
</body>
</html>
