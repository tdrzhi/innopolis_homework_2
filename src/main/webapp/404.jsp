<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% request.setAttribute("title", "Information System"); %>
<html>
<%@ include file="layout/head.jsp" %>
<body>
<%@ include file="layout/header.jsp" %>
<div class="container-fluid">
    <h1 class="text-center h1">PAGE NOT FOUND</h1>
    <h2 class="h2 text-center"><a href="/">To home</a></h2>
</div>
<%@ include file="layout/footer.jsp" %>
</body>
</html>
