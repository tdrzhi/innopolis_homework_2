package tdorzhi.homework2.pojo;

import org.springframework.context.annotation.Bean;

/**
 * Created by dixi on 12/31/16.
 *
 */
public class Null implements Nullable{
    private static Nullable nullable;
    private static Nullable[] nullables;

    public boolean isNull() {
        return true;
    }

    public static Nullable getNull() {
        if (nullable == null) {
            nullable = new Null();
        }
        return nullable;
    }

    public static Nullable[] getNulls() {
        if (nullables == null) {
            nullables = new Nullable[]{getNull()};
        }
        return nullables;
    }

}
