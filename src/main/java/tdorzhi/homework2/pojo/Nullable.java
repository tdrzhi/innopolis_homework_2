package tdorzhi.homework2.pojo;

/**
 * Created by dixi on 12/31/16.
 */
public interface Nullable {
    boolean isNull();
}
