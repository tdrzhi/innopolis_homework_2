package tdorzhi.homework2.pojo;

import tdorzhi.homework2.utils.Password;

/**
 * Created by dixi on 23.12.16.
 *
 */
public class User implements Nullable{

    private int id;
    private boolean enabled;
    private String name;

    private String email;

    private String password;

    public User(int id, boolean enabled, String name, String email, Password password) {
        this.id = id;
        this.enabled = enabled;
        this.name = name;
        this.email = email;
        this.password = password.getHash();
    }

    public User() {}

    @Override
    public boolean isNull() {
        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password.getHash();
    }
}
