package tdorzhi.homework2.pojo;

/**
 * Created by dixi on 23.12.16.
 *
 */
public class Item implements Nullable{
    private int id;

    private boolean enabled;
    private String name;
    private String budget;
    private int listId;

    public Item() {
    }

    public Item(int id, boolean enabled, String name, String budget, int listId) {
        this.id = id;
        this.enabled = enabled;
        this.name = name;
        this.budget = budget;
        this.listId = listId;
    }

    @Override
    public boolean isNull() {
        return false;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }
}
