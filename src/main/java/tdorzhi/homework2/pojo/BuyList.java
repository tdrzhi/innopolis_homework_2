package tdorzhi.homework2.pojo;

/**
 * Created by dixi on 23.12.16.
 *
 */
public class BuyList implements Nullable {

    private int id;
    private boolean enabled;
    private String name;
    private int userId;

    public BuyList(int id, boolean enabled, String name, int userId) {
        this.id = id;
        this.enabled = enabled;
        this.name = name;
        this.userId = userId;
    }

    public BuyList() {}

    @Override
    public boolean isNull() {
        return false;
    }

    public int getId() {
        return id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
