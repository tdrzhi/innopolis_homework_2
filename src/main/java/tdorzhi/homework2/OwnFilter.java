package tdorzhi.homework2;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by dixi on 12/26/16.
 *
 */
@WebFilter("/*")
public class OwnFilter implements Filter {
    private static Set<String> whiteList = new HashSet<>();
    private static Set<String> authorizedOnly = new HashSet<>();
    private static Set<String> guestsOnly = new HashSet<>();

    static {
        whiteList.add("/");
        whiteList.add("/webjars/.*(\\.css|\\.js|\\.png|\\.jpg)");
        whiteList.add("/public/.*(\\.css|\\.js|\\.png|\\.jpg)");
        whiteList.add("/404");

        guestsOnly.add("/user/(login|register)");

        authorizedOnly.add("/user/logout");
        // this complicated regex is made to make sure we can't receive query like /list/update/doSome
        // only /list/update/123 or /list/update
        authorizedOnly.add("/list/(update|delete|create)(/-?[\\d]+$|/?$)");
        authorizedOnly.add("/item/(update|delete|create)(/-?[\\d]+$|/?$)");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);

        String query = httpRequest.getRequestURI();
        boolean loggedIn = session != null && session.getAttribute("logged") != null;

        if (hasMatchingElem(whiteList, query)) {
            chain.doFilter(httpRequest, httpResponse);
        } else if (!loggedIn && hasMatchingElem(guestsOnly, query)) {
            chain.doFilter(httpRequest, httpResponse);
        } else if (loggedIn && hasMatchingElem(authorizedOnly, query)) {
                chain.doFilter(httpRequest, httpResponse);
        } else {
                httpResponse.sendRedirect("/404");  //  we did not found page in determined page sets...
        }
    }

    @Override
    public void destroy() {

    }

    private boolean hasMatchingElem(Set<String> set, String toMatch) {
        for (String string : set) {
            if (toMatch.matches(string)) {
                return true;
            }
        }
        return false;
    }
}
