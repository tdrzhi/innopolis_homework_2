package tdorzhi.homework2.utils;

import org.mindrot.jbcrypt.BCrypt;

/**
 * Created by dixi on 1/5/17.
 *
 */
public class Password {

    private String hash;

    public Password(String hash) {
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public static Password hash(String toHash, String salt) {
        return new Password(BCrypt.hashpw(toHash, salt));
    }

    public static Password hash(String toHash) {
        return hash(toHash, BCrypt.gensalt());
    }

    public static boolean checkPass(String plainPass, String hashedPass) {
        return BCrypt.checkpw(plainPass, hashedPass);
    }
}
