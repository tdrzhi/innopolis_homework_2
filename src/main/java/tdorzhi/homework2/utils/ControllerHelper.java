package tdorzhi.homework2.utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by dixi on 1/2/17.
 *
 */
public class ControllerHelper {
    private static String errorPage = "/error.jsp";

    public static void ErrorHandler(Exception e, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("exception", e);
        req.getRequestDispatcher(errorPage).forward(req, resp);
    }

}
