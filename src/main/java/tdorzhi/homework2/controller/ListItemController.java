package tdorzhi.homework2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.HttpRequestHandler;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.model.service.ICRUDService;
import tdorzhi.homework2.model.service.ItemService;
import tdorzhi.homework2.model.service.ListService;
import tdorzhi.homework2.model.service.UserService;
import tdorzhi.homework2.pojo.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static tdorzhi.homework2.utils.ControllerHelper.ErrorHandler;


/**
 * Created by dixi on 12/25/16.
 *
 */
public class ListItemController implements HttpRequestHandler {

    private ListService listService;
    private ItemService itemService;
    private UserService userService;

    @Autowired
    public void setListService(ListService listService) {
        this.listService = listService;
    }

    @Autowired
    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect("/");
    }

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("POST".equals(request.getMethod())) {
            doPost(request, response);
        } else if ("GET".equals(request.getMethod())) {
            doGet(request, response);
        }
    }

    private void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        User currentUser = (User) req.getSession().getAttribute("logged");
        String[] query = req.getRequestURI() != null ? req.getRequestURI().split("/") : new String[]{};
        String entity = query[1];
        String action = query[2];
        int entityId = Integer.parseInt(query[3]);
        ICRUDService service;
        Nullable nullable;
        if ("item".equals(entity)) {
            service = itemService;
            nullable = new Item(
                    entityId,
                    req.getParameter("enabled") == null
                            || Boolean.parseBoolean(req.getParameter("enabled")),
                    req.getParameter("name"),
                    req.getParameter("budget"),
                    req.getParameter("listId") != null
                            ? Integer.parseInt(req.getParameter("listId"))
                            : -1);
        } else if ("list".equals(entity)) {
            service = listService;
            nullable = new BuyList(
                    entityId,
                    req.getParameter("enabled") == null
                            || Boolean.parseBoolean(req.getParameter("enabled")),
                    req.getParameter("name"),
                    currentUser.getId());
        } else {
            resp.sendRedirect("/");
            return;
        }
        try {
            if (performCRUD(action, service, nullable)) {
                req.getSession().setAttribute("map", userService.getLists(currentUser.getId()));
            }
        } catch (FatalException | RecordExistsException | RecordNotFoundException e) {
            ErrorHandler(e, req,resp);
            return;
        }
        resp.sendRedirect("/");
    }

    private static boolean performCRUD(String action,
                                      ICRUDService service,
                                      Nullable nullable)
            throws FatalException, RecordExistsException {
        boolean success = false;
        switch (action) {
            case "delete":
                success = service.delete(nullable);
                break;
            case "create":
                success = service.create(nullable);
                break;
            case "update":
                success = service.update(nullable);
                break;
        }
        return success;
    }

}