package tdorzhi.homework2.controller;

import org.springframework.web.context.support.HttpRequestHandlerServlet;

import javax.servlet.annotation.WebServlet;

/**
 * Created by dixi on 1/11/17.
 *
 */

@WebServlet(name = "listItemServlet", urlPatterns = {"/item/*", "/list/*"})
public class ListItemServlet extends HttpRequestHandlerServlet {

}
