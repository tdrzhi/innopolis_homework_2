package tdorzhi.homework2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.HttpRequestHandler;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.pojo.Nullable;
import tdorzhi.homework2.pojo.User;
import tdorzhi.homework2.model.service.UserService;
import tdorzhi.homework2.utils.ControllerHelper;
import tdorzhi.homework2.utils.Password;

/**
 * Created by dixi on 12/20/16.
 *
 */
public class UserController implements HttpRequestHandler {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] query = request.getPathInfo().split("/"); // /register
        String action = query[1];
        String redirectTo = "/";
        try {
            Nullable user;
            switch (action) {
                case "login":
                    user = userService.login(request.getParameter("email"), request.getParameter("password"));
                    if (!user.isNull()) {
                        request.getSession().setAttribute("logged", user);
                        request.getSession().setMaxInactiveInterval(900); //15 mins
                        request.getSession().setAttribute("map", userService.getLists(((User) user).getId()));
                    } else {
                        redirectTo = "/user/login";
                    }
                    break;
                case "register":
                    String name = request.getParameter("name");
                    String email = request.getParameter("email");
                    Password password = new Password(request.getParameter("password"));
                    user = userService.register(new User(-1, true, name, email, password));
                    if (!user.isNull()) {
                        request.getSession().setAttribute("logged", user);
                        request.getSession().setMaxInactiveInterval(900); //15 mins
                    } else {
                        redirectTo = "/user/register";
                    }
                    break;
                case "logout":
                    request.getSession().invalidate();
                    break;
            }
        } catch (RecordExistsException | RecordNotFoundException | FatalException e){
            log.error(e.getMessage());
            ControllerHelper.ErrorHandler(e, request, response);
            return;
        }
        response.sendRedirect(redirectTo);
    }

    private void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] query = request.getPathInfo().split("/");
        String toDispatch = "/";
        String action = query[1];

        if ("login".equals(action)) {
            toDispatch = "/layout/user/login.jsp";
        } else if ("register".equals(action)) {
            toDispatch = "/layout/user/register.jsp";
        }
        request.getRequestDispatcher(toDispatch).forward(request, response);
    }

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if ("POST".equals(request.getMethod())) {
            doPost(request, response);
        } else if ("GET".equals(request.getMethod())) {
            doGet(request, response);
        }
    }
}