package tdorzhi.homework2.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.exception.FatalException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by dixi on 22.12.16.
 *
 */
public class DBConnection {
    private static final Logger log = LoggerFactory.getLogger(DBConnection.class);
    private static DataSource dataSource;

    // TODO: use spring-jdbc instead
    private static void init() throws NamingException {
        InitialContext context = new InitialContext();
        DBConnection.dataSource = (DataSource) context.lookup("java:comp/env/jdbc/h2");
    }

    public static Connection getConnection() throws FatalException {
        try {
            if (dataSource == null) {
                init();
            }
            return dataSource.getConnection();
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            throw new FatalException(e);
        }
    }
}
