package tdorzhi.homework2.exception;

import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Created by dixi on 1/3/17.
 *
 */
public class FatalException extends Exception {
    private Throwable rootCause = null;

    public FatalException(Throwable e) {
        super("Critical error occured", e);
        rootCause = e;
    }

    public FatalException(String message, Throwable e) {
        super(message, e);
        rootCause = e;
    }

    @Override
    public synchronized Throwable getCause() {
        return rootCause;
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
        if (rootCause != null) {
            rootCause.printStackTrace();
        }
    }

    @Override
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
        if (rootCause != null) {
            rootCause.printStackTrace(s);
        }
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
        if (rootCause != null) {
            rootCause.printStackTrace(s);
        }
    }
}
