package tdorzhi.homework2.exception;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * Created by dixi on 1/2/17.
 *
 */
public class RecordExistsException extends SQLException {
    private Throwable rootCause = null;

    public RecordExistsException(String message, SQLException e) {
        super(message, e);
        rootCause = e;
    }

    @Override
    public synchronized Throwable getCause() {
        return rootCause;
    }

    @Override
    public void printStackTrace() {
        super.printStackTrace();
        if (rootCause != null) {
            rootCause.printStackTrace();
        }
    }

    @Override
    public void printStackTrace(PrintStream s) {
        super.printStackTrace(s);
        if (rootCause != null) {
            rootCause.printStackTrace(s);
        }
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        super.printStackTrace(s);
        if (rootCause != null) {
            rootCause.printStackTrace(s);
        }
    }
}
