package tdorzhi.homework2.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.pojo.Nullable;
import tdorzhi.homework2.pojo.User;
import tdorzhi.homework2.utils.Password;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * Created by dixi on 12/23/16.
 * UserModel's DAO
 */
public class UserModel extends AbstractModel<User> {
    private static final Logger log = LoggerFactory.getLogger(UserModel.class);

    private static final String GET_BY_ID = "SELECT id, ENABLED, name, email, password FROM user WHERE id = ?";
    private static final String GET_BY_EMAIL = "SELECT id, ENABLED, name, email, password FROM user WHERE email = ?";
    private static final String GET_ALL = "SELECT id, ENABLED, name, email, password FROM user";
    private static final String UPDATE = "UPDATE user SET ENABLED = ?, name = ?, email = ?, password = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM user WHERE id = ?";
    private static final String CREATE = "INSERT INTO user(ENABLED, name, email, password) VALUES(?, ?, ?, ?)";

    User assignFields(ResultSet set) throws SQLException {
        Password password = new Password(set.getString(5));
        return new User(set.getInt(1),
                set.getBoolean(2),
                set.getString(3),
                set.getString(4),
                password);
    }

    @Override
    public User getById(int value) throws FatalException {
        return (User) getById(GET_BY_ID, value);
    }

    @Override
    public Nullable getBy(Object email) throws RecordNotFoundException, FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(GET_BY_EMAIL)){
            stmt.setString(1, (String) email);
            ResultSet set = stmt.executeQuery();
            set.next();
            return assignFields(set);
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RecordNotFoundException("User with email " + email + " not found", e);
        }
    }

    @Override
    public List<User> getManyBy(Object email) throws RecordNotFoundException, FatalException {
        return Collections.emptyList();
    }

    @Override
    public List<User> getAll() throws FatalException {
        return getAll(GET_ALL);
    }

    @Override
    public int update(User toUpdate) throws FatalException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(UPDATE)){
            stmt.setBoolean(1, toUpdate.isEnabled());
            stmt.setString(2, toUpdate.getName());
            stmt.setString(3, toUpdate.getEmail());
            stmt.setString(4, toUpdate.getPassword());
            stmt.setInt(5, toUpdate.getId());

            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(int id) throws FatalException {
        return delete(DELETE, id);
    }

    @Override
    public int deleteBy(Object userId) throws FatalException {
        return delete(DELETE, (Integer) userId);
    }

    @Override
    public int create(User toCreate) throws RecordExistsException, FatalException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(CREATE)) {
            stmt.setBoolean(1, toCreate.isEnabled());
            stmt.setString(2, toCreate.getName());
            stmt.setString(3, toCreate.getEmail());
            stmt.setString(4, toCreate.getPassword());

            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            if ("23505".equals(e.getSQLState())) { //we got duplicate key violation, rewatch SQL states
                throw new RecordExistsException("User with email " + toCreate.getEmail() + " exists", e);
            }
            throw new FatalException(e);
        }
    }
}