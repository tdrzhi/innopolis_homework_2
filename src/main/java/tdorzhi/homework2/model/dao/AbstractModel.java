package tdorzhi.homework2.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tdorzhi.homework2.database.DBConnection;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.pojo.Null;
import tdorzhi.homework2.pojo.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by dixi on 12/23/16.
 *
 */
public abstract class AbstractModel<T extends Nullable> {
    private static Logger log = LoggerFactory.getLogger(AbstractModel.class);

    @Autowired
    protected Connection getConnection() throws FatalException {
        return DBConnection.getConnection();
    }

    Nullable getById(String statement, int value) throws FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(statement)){
            stmt.setInt(1, value);
            ResultSet result = stmt.executeQuery();
            result.next();
            return assignFields(result);
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Null.getNull();
    }

    List<T> getAll(String statement) throws FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(statement)){
            ResultSet set = stmt.executeQuery();
            List<T> list = new ArrayList<>();
            while (set.next()) {
                list.add(assignFields(set));
            }
            return list;
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return Collections.emptyList();
    }

    int delete(String statement, int value) throws FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(statement)){
            stmt.setInt(1, value);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    abstract T assignFields(ResultSet set) throws SQLException;

    public abstract Nullable getById(int value) throws FatalException;
    public abstract Nullable getBy(Object field) throws RecordNotFoundException, FatalException;
    public abstract List<T> getManyBy(Object field) throws RecordNotFoundException, FatalException;
    public abstract List<T> getAll() throws FatalException;
    public abstract int update(T toUpdate) throws FatalException;
    public abstract int delete(int id) throws FatalException;
    public abstract int deleteBy(Object field) throws FatalException;

    public abstract int create(T toCreate) throws RecordExistsException, FatalException;
}
