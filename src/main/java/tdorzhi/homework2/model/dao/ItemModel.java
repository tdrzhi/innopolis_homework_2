package tdorzhi.homework2.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.pojo.Item;
import tdorzhi.homework2.pojo.Null;
import tdorzhi.homework2.pojo.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dixi on 12/23/16.
 * ItemModel's DAO
 */
public class ItemModel extends AbstractModel<Item> {
    private static final Logger log = LoggerFactory.getLogger(ItemModel.class);

    private static final String GET_BY_ID = "SELECT id, ENABLED, name, budget, listId FROM item WHERE id = ?";
    private static final String GET_BY_LISTID = "SELECT id, ENABLED, name, budget, listId FROM item WHERE listId = ?";
    private static final String GET_ALL = "SELECT id, ENABLED, name, budget, listId FROM item";
    private static final String UPDATE = "UPDATE item SET ENABLED = ?, name = ?, budget = ?, listId = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM item WHERE id = ?";
    private static final String DELETE_BY_LISTID = "DELETE FROM item WHERE listId = ?";
    private static final String CREATE = "INSERT INTO item(ENABLED, name, budget, listId) VALUES(?, ?, ?, ?)";

    Item assignFields(ResultSet set) throws SQLException {
        return new Item(
                set.getInt(1),      //id
                set.getBoolean(2),  //status
                set.getString(3),   //name
                set.getString(4),   //budget
                set.getInt(5));     //listId
    }

    @Override
    public Item getById(int value) throws FatalException {
        return (Item) getById(GET_BY_ID, value);
    }

    @Override
    public Nullable getBy(Object field) throws RecordNotFoundException, FatalException {
        return Null.getNull();
    }

    @Override
    public List<Item> getManyBy(Object listId) throws RecordNotFoundException, FatalException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(GET_BY_LISTID)){
            stmt.setInt(1, (Integer) listId);
            ResultSet set = stmt.executeQuery();
            List<Item> list = new ArrayList<>();
            while (set.next()) {
                list.add(assignFields(set));
            }
            return list;
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RecordNotFoundException(e.getMessage(), e);
        }
    }

    @Override
    public List<Item> getAll() throws FatalException {
        return getAll(GET_ALL);
    }

    @Override
    public int update(Item toUpdate) throws FatalException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(UPDATE)){
            stmt.setBoolean(1, toUpdate.isEnabled());
            stmt.setString(2, toUpdate.getName());
            stmt.setString(3, toUpdate.getBudget());
            stmt.setInt(4, toUpdate.getListId());
            stmt.setInt(5, toUpdate.getId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(int id) throws FatalException {
        return delete(DELETE, id);
    }

    @Override
    public int deleteBy(Object field) throws FatalException {
        return delete(DELETE_BY_LISTID, (Integer) field);
    }

    @Override
    public int create(Item toCreate) throws RecordExistsException, FatalException {
        try (Connection connection = getConnection();
             PreparedStatement stmt = connection.prepareStatement(CREATE)){
            stmt.setBoolean(1, toCreate.isEnabled());
            stmt.setString(2, toCreate.getName());
            stmt.setString(3, toCreate.getBudget());
            stmt.setInt(4, toCreate.getListId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RecordExistsException(e.getMessage(), e);
        }
    }
}