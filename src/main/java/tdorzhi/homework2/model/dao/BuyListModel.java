package tdorzhi.homework2.model.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.pojo.BuyList;
import tdorzhi.homework2.pojo.Null;
import tdorzhi.homework2.pojo.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by dixi on 12/23/16.
 * BuyListModel's DAO
 */
public class BuyListModel extends AbstractModel<BuyList> {
    private static final Logger log = LoggerFactory.getLogger(BuyListModel.class);

    private static final String GET_BY_ID = "SELECT id, enabled, name, userId FROM list WHERE id = ?";
    private static final String GET_BY_USERID = "SELECT id, enabled, name, userId FROM list WHERE userId = ?";
    private static final String GET_ALL = "SELECT id, enabled, name, userId FROM list";
    private static final String UPDATE = "UPDATE list SET enabled = ?, name = ?, userId = ? WHERE id = ?";
    private static final String DELETE = "DELETE FROM list WHERE id = ?";
    private static final String DELETE_BY_USERID = "DELETE FROM list WHERE userId = ?";
    private static final String CREATE = "INSERT INTO list(enabled, name, userId) VALUES(?, ?, ?)";

    @Override
    BuyList assignFields(ResultSet set) throws SQLException {
        return new BuyList(set.getInt(1), set.getBoolean(2),
                set.getString(3), set.getInt(4));
    }

    @Override
    public BuyList getById(int value) throws FatalException {
        return (BuyList) getById(GET_BY_ID, value);
    }

    @Override
    public Nullable getBy(Object field) throws RecordNotFoundException, FatalException {
        return Null.getNull();
    }

    @Override
    public List<BuyList> getManyBy(Object userId) throws RecordNotFoundException, FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(GET_BY_USERID)){
            stmt.setInt(1, (Integer) userId);
            ResultSet set = stmt.executeQuery();
            List<BuyList> list = new ArrayList<>();
            while (set.next()) {
                list.add(assignFields(set));
            }
            return list;
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RecordNotFoundException(e.getMessage(), e);
        }
    }

    @Override
    public List<BuyList> getAll() throws FatalException {
        return getAll(GET_ALL);
    }

    @Override
    public int update(BuyList toUpdate) throws FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(UPDATE)){
            stmt.setBoolean(1, toUpdate.isEnabled());
            stmt.setString(2, toUpdate.getName());
            stmt.setInt(3, toUpdate.getUserId());
            stmt.setInt(4, toUpdate.getId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
        return 0;
    }

    @Override
    public int delete(int id) throws FatalException {
        return delete(DELETE, id);
    }

    @Override
    public int deleteBy(Object field) throws FatalException {
        return delete(DELETE_BY_USERID, (Integer) field);
    }

    @Override
    public int create(BuyList toCreate) throws RecordExistsException, FatalException {
        try (Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(CREATE)){
            stmt.setBoolean(1, toCreate.isEnabled());
            stmt.setString(2, toCreate.getName());
            stmt.setInt(3, toCreate.getUserId());
            return stmt.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage());
            throw new RecordExistsException(e.getMessage(), e);
        }
    }
}