package tdorzhi.homework2.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.exception.RecordNotFoundException;
import tdorzhi.homework2.model.dao.AbstractModel;
import tdorzhi.homework2.pojo.*;
import tdorzhi.homework2.utils.Password;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dixi on 12/27/16.
 *
 */
public class UserService {
    private AbstractModel<User> userModel;
    private AbstractModel<BuyList> listModel;
    private AbstractModel<Item> itemModel;

    public Nullable login(String email, String password) throws RecordNotFoundException, FatalException {
        if (email != null &&  password != null) {
            Nullable user = userModel.getBy(email);
            if (!user.isNull() && Password.checkPass(password, ((User) user).getPassword())) {
                return user;
            }
        }
        return Null.getNull();
    }

    public Nullable register(Nullable nullable) throws RecordExistsException, RecordNotFoundException, FatalException {
        if (!nullable.isNull() && validate((User) nullable)) {
            User realUser = (User) nullable;
            Password password = Password.hash(realUser.getPassword());
            realUser.setPassword(password);
            if (userModel.create(realUser) > 0) {
                return userModel.getBy(realUser.getEmail());
            }
        }
        return Null.getNull();
    }

    private boolean validate(User user) {
        // assuming we get PBKDF2HmacSHA1 16 byte hashed pass
//simplest regex I could find
        return !user.isNull() &&
                user.getEmail() != null &&
                !user.getEmail().isEmpty() &&
                user.getPassword() != null &&
                user.getPassword().length() > 31 &&
                user.getEmail().matches(".+@.+\\.\\w{2,}");
    }

    public boolean block(int id) throws FatalException {
        User user = (User) userModel.getById(id);
        user.setEnabled(false);
        return userModel.update(user) > 0;
    }

    public boolean unblock(int id) throws FatalException {
        User user = (User) userModel.getById(id);
        user.setEnabled(true);
        return userModel.update(user) > 0;
    }

    public Map<BuyList, List<Item>> getLists(int userId) throws RecordNotFoundException, FatalException {
        List<BuyList> lists = listModel.getManyBy(userId);
        Map<BuyList, List<Item>> map = new HashMap<>(lists.size());

        for (BuyList list : lists) {
            map.put(list, itemModel.getManyBy(list.getId()));
        }

        return map;
    }

    @Autowired
    public void setUserModel(AbstractModel<User> userModel) {
        this.userModel = userModel;
    }

    @Autowired
    public void setListModel(AbstractModel<BuyList> listModel) {
        this.listModel = listModel;
    }

    @Autowired
    public void setItemModel(AbstractModel<Item> itemModel) {
        this.itemModel = itemModel;
    }
}
