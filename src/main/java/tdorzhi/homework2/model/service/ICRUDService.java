package tdorzhi.homework2.model.service;

import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.pojo.Nullable;

/**
 * Created by dixi on 1/3/17.
 *
 */
public interface ICRUDService {
    boolean delete(Nullable toDelete) throws FatalException;
    boolean create(Nullable toCreate) throws RecordExistsException, FatalException;
    boolean update(Nullable toUpdate) throws FatalException;
}
