package tdorzhi.homework2.model.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.model.dao.AbstractModel;
import tdorzhi.homework2.model.dao.BuyListModel;
import tdorzhi.homework2.model.dao.ItemModel;
import tdorzhi.homework2.pojo.BuyList;
import tdorzhi.homework2.pojo.Item;
import tdorzhi.homework2.pojo.Nullable;

/**
 * Created by dixi on 12/27/16.
 *
 */
public class ListService implements ICRUDService {
    private static Logger log = LoggerFactory.getLogger(ListService.class);

    private AbstractModel<BuyList> listModel;
    private AbstractModel<Item> itemModel;

    @Autowired
    public void setListModel(BuyListModel listModel) {
        this.listModel = listModel;
    }

    @Autowired
    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    private boolean validate(Nullable list) {
        return !list.isNull()
                && ((BuyList) list).getName() != null
                && !((BuyList) list).getName().isEmpty();
    }

    @Override
    public boolean delete(Nullable list) throws FatalException {
        if (!list.isNull()) {
            BuyList realList = (BuyList) list;
            itemModel.deleteBy(realList.getId()); //clear out foreign key dependents
            return listModel.delete(realList.getId()) > 0;
        }
        return false;
    }

    @Override
    public boolean create(Nullable list) throws RecordExistsException, FatalException {
        if (!list.isNull()) {
            BuyList realList = (BuyList) list;
            return validate(realList) && listModel.create(realList) > 0;
        }
        return false;
    }

    @Override
    public boolean update(Nullable list) throws FatalException {
        if (!list.isNull()) {
            BuyList realList = (BuyList) list;
            return validate(realList) && listModel.update(realList) > 0;
        }
        return false;
    }
}
