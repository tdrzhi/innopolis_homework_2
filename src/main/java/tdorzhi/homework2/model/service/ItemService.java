package tdorzhi.homework2.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import tdorzhi.homework2.exception.FatalException;
import tdorzhi.homework2.exception.RecordExistsException;
import tdorzhi.homework2.model.dao.AbstractModel;
import tdorzhi.homework2.model.dao.ItemModel;
import tdorzhi.homework2.pojo.Item;
import tdorzhi.homework2.pojo.Nullable;


/**
 * Created by dixi on 12/27/16.
 *
 */
public class ItemService implements ICRUDService {
    private AbstractModel<Item> itemModel;

    @Autowired
    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    private boolean validate(Item item) {
        return item != null && item.getName() != null && !item.getName().isEmpty() && item.getListId() != -1;
    }

    @Override
    public boolean create(Nullable item) throws RecordExistsException, FatalException {
        if (!item.isNull()) {
            Item realItem = (Item) item;
            return validate(realItem) && itemModel.create(realItem) > 0;
        }
        return false;
    }

    @Override
    public boolean delete(Nullable item) throws FatalException {
        return !item.isNull() && itemModel.delete(((Item) item).getId()) > 0;
    }

    @Override
    public boolean update(Nullable item) throws FatalException {
        if (!item.isNull()) {
            Item newItem = (Item) item;
            Nullable nullable = itemModel.getById(newItem.getId());
            if (!nullable.isNull()) {
                Item realItem = (Item) nullable;
                if (newItem.getName() != null && !newItem.getName().isEmpty() && !realItem.isNull()) {
                    realItem.setName(newItem.getName());
                }
                if (newItem.getBudget() != null && !newItem.getBudget().isEmpty() && !realItem.isNull()) {
                    realItem.setBudget(newItem.getBudget());
                }
                ((Item) nullable).setEnabled(newItem.isEnabled());
                return validate(realItem) && itemModel.update(realItem) > 0;
            }
        }
        return false;
    }
}
