package tdorzhi.homework2.model.dao;

import org.junit.*;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.pojo.BuyList;
import tdorzhi.homework2.pojo.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static tdorzhi.homework2.database.DBConnection.getConnection;

/**
 * Created by dixi on 23.12.16.
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ListModelTest {
    private static Connection connection;
    private static Logger log = LoggerFactory.getLogger(ListModelTest.class);
    private static AbstractModel<BuyList> listModel;
    private static AbstractModel<User> userModel;
    private static BuyList list;
    private static User user;

    static {
        try {
            connection = getConnection();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
    @BeforeClass
    public static void init() {
        listModel = new BuyListModel();
        userModel = new UserModel();

        userModel.create(new User(0, true, "name", "user@example.com", "pass"));
        user = userModel.getBy("user@example.com")[0];

        listModel.create(new BuyList(0, true, "name", user.getId()));
        list = listModel.getBy(user.getId())[0];
    }

    @Test
    public void AgetAllTest() {
        BuyList[] lists = listModel.getAll();
        Assert.assertNotNull(lists);
        Assert.assertTrue(lists.length > 0);
    }

    @Test
    public void BgetByIdTest() {
        BuyList buyList = listModel.getById(list.getId());
        Assert.assertNotNull(buyList);
    }

    @Test
    public void CgetByTest() {
        BuyList buyList = listModel.getBy(user.getId())[0];
        Assert.assertNotNull(buyList);
    }

    @Test
    public void DupdateTest() {
        list.setName("updated!!");
        int affectedRows = listModel.update(list);
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void EdeleteTest() {
        int affectedRows = listModel.delete(list.getId());
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void FcreateTest() {
        int affectedRows = listModel.create(list);
        Assert.assertTrue(affectedRows > 0);
    }

    @AfterClass
    public static void end() {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("DELETE FROM list");
            stmt.executeUpdate("DELETE FROM user");
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
}
