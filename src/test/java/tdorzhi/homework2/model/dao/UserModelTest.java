package tdorzhi.homework2.model.dao;

import org.junit.*;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.pojo.User;

import java.sql.*;

import static tdorzhi.homework2.database.DBConnection.getConnection;

/**
 * Created by dixi on 23.12.16.
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserModelTest {
    private static Connection connection;
    private static Logger log = LoggerFactory.getLogger(UserModelTest.class);
    private static AbstractModel<User> userModel;
    private static User user;

    static {
        try {
            connection = getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public static void init() {
        userModel = new UserModel();
        userModel.create(new User(0, true, "name", "user@example.com", "pass"));
        user = userModel.getBy("user@example.com")[0];
    }

    @Test
    public void AgetAllTest() {
        User[] users = userModel.getAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.length > 0);
    }

    @Test
    public void BgetByIdTest() {
        User user1 = userModel.getById(user.getId());
        Assert.assertNotNull(user);
    }

    @Test
    public void CgetByTest() {
        User user1 = userModel.getBy(user.getEmail())[0];
        Assert.assertNotNull(user);
    }

    @Test
    public void DupdateTest() {
        user.setName("updated!!");
        int affectedRows = userModel.update(user);
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void EdeleteTest() {
        int affectedRows = userModel.delete(user.getId());
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void FcreateTest() {
        int affectedRows = userModel.create(user);
        Assert.assertTrue(affectedRows > 0);
    }

    @AfterClass
    public static void end() {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            int deletedTestUser = stmt.executeUpdate("DELETE FROM user");
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
}
