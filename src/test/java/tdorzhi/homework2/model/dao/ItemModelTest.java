package tdorzhi.homework2.model.dao;

import org.junit.*;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tdorzhi.homework2.pojo.BuyList;
import tdorzhi.homework2.pojo.Item;
import tdorzhi.homework2.pojo.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static tdorzhi.homework2.database.DBConnection.getConnection;

/**
 * Created by dixi on 23.12.16.
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ItemModelTest {
    private static Connection connection;
    private static Logger log = LoggerFactory.getLogger(ItemModelTest.class);
    private static AbstractModel<BuyList> listModel;
    private static AbstractModel<User> userModel;
    private static AbstractModel<Item> itemModel;
    private static BuyList list;
    private static User user;
    private static Item item;

    static {
        try {
            connection = getConnection();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
    @BeforeClass
    public static void init() {
        listModel = new BuyListModel();
        userModel = new UserModel();
        itemModel = new ItemModel();

        userModel.create(new User(0, true, "user name", "user@example.com", "pass"));
        user = userModel.getBy("user@example.com")[0];

        listModel.create(new BuyList(0, true, "list name", user.getId()));
        list = listModel.getBy(user.getId())[0];

        itemModel.create(new Item(0, true, "item name", "item desc", "100000$", list.getId()));
        item = itemModel.getBy(list.getId())[0];
    }

    @Test
    public void AgetAllTest() {
        Item[] items = itemModel.getAll();
        Assert.assertNotNull(items);
        Assert.assertTrue(items.length > 0);
    }

    @Test
    public void BgetByIdTest() {
        Item item1 = itemModel.getById(item.getId());
        Assert.assertNotNull(item1);
    }

    @Test
    public void CgetByTest() {
        Item item1 = itemModel.getBy(list.getId())[0];
        Assert.assertNotNull(item1);
    }

    @Test
    public void DupdateTest() {
        item.setName("updated!!");
        int affectedRows = itemModel.update(item);
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void EdeleteTest() {
        int affectedRows = itemModel.delete(item.getId());
        Assert.assertTrue(affectedRows > 0);
    }

    @Test
    public void FcreateTest() {
        int affectedRows = itemModel.create(item);
        Assert.assertTrue(affectedRows > 0);
    }

    @AfterClass
    public static void end() {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("DELETE FROM item");
            stmt.executeUpdate("DELETE FROM list");
            stmt.executeUpdate("DELETE FROM user");
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
}
